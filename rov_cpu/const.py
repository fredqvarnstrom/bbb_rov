# Button assignments
buttons = {'A': ['left', 'forward'],
           'B': ['right', 'forward'],
           'X': ['right', 'backward'],
           'Y': ['right', 'forward']}
latency = 20000  # millisec
ts = None


left_wheel_state = "stop"  # forward, reverse, stop
right_wheel_state = "stop"  # forward, reverse, stop
