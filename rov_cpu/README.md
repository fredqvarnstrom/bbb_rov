## Websocket and Rover Commands

#### Commands:

###### *{'componentType':'Button' , 'Button':button,'Action':on_off}*

* componentType: 'Button'
* Button: A, B, X, Y
* Action: on, off

###### *{componentType': 'ThumbStick', 'ThumbStick': thumbstick, 'Direction': direction, 'LValue': <int>, 'RValue': <int>}*

* componentType: 'ThumbStick'
* ThumbStick: left, right
* Direction: forward, reverse
* LValue: <int>
* RValue: <int>