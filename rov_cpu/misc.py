import time

from datetime import datetime
from rover_commands import RoverCommands

import const as C


def command_watcher():
    print "starting watcher..."
    cmd = RoverCommands()

    while True:
        time.sleep(0.01)
        try:
            ts = datetime.now()
            ts_old = C.ts

            if (ts - ts_old).microseconds/1000 > C.latency:
                print "stopping motor..."
                cmd.stop_motor()
                C.ts = None
        except:
            pass
