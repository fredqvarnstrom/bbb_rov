"""
 This is a sample client for websocket
"""

import websocket
import time

if __name__ == "__main__":

    websocket.enableTrace(True)
    ws = websocket.create_connection("ws://216.177.177.179:5555/rov/control")
    print("Sending message.")

    n = 0

    while True:
        n += 1

        #ws.send("{'componentType':'Button' , 'Button': 'A' , 'Action': 'on'}")
        #ws.send("{'componentType':'Button' , 'Button': 'X' , 'Action': 'on'}")
        #if n >= 100:
        #    ws.send("{'componentType':'Button' , 'Button': 'X' , 'Action': 'off'}")
        #else:
        #    ws.send("{'componentType':'Button' , 'Button': 'X' , 'Action': 'on'}")
        #ws.send("{'componentType':'ThumbStick' , 'ThumbStick': 'left' , 'Direction': 'forward', 'LValue': 100, 'RValue': 20}")
        ws.send("{'componentType':'ThumbStick' , 'ThumbStick': 'left' , 'Direction': 'reverse', 'LValue': "+str(n)+", 'RValue': 20}")

        time.sleep(0.05)
    #print("Sent")
    #print("Receiving...")
    #result = ws.recv()
    #print("Received: {}".format(result))
    #ws.close()
