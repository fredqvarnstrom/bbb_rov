import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.ioloop
import threading

import const as C
import misc

from datetime import datetime
from controller_actions import *


class WebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print 'Opening connection...'
        pass

    def on_message(self, message):
        self.change_ts(datetime.now())

        try:
            message = eval(message)

            component_type = message['componentType'].lower()

            if component_type == "button":
                button = message['Button']
                action = message['Action']

                button_actions = ButtonActions()
                button_actions.parse_button_action(button, action)
                button_actions.button_action()
            elif component_type == "trigger":
                # I think this should be handled by the c++ code
                pass

            elif component_type == "thumbstick":
                # {'componentType': 'ThumbStick',
                # 'ThumbStick': 'left/right',
                # 'Direction': 'forward/reverse',
                # 'LValue': <int>,
                # 'RValue': <int>}

                if message['ThumbStick'] == 'left':
                    thumbstick = message['ThumbStick']
                    direction = message['Direction']
                    lvalue = message['LValue']
                    rvalue = message['RValue']

                    thumbstick_actions = ThumbStickActions()

                    if direction == 'forward':
                        thumbstick_actions.thumbstick_action(thumbstick, 'left-forward', lvalue)
                        thumbstick_actions.thumbstick_action(thumbstick, 'right-forward', rvalue)
                    elif direction == 'reverse':
                        thumbstick_actions.thumbstick_action(thumbstick, 'left-reverse', lvalue)
                        thumbstick_actions.thumbstick_action(thumbstick, 'right-reverse', rvalue)

            # here should be motor action

        except Exception as e:
            print e
            pass

    def on_close(self):
        cmd = RoverCommands()
        cmd.stop_motor()
        print 'Closing...'
        pass

    def change_ts(self, ts):
        while True:
            try:
                C.ts = ts
                break
            except:
                pass


class App(tornado.web.Application):
    def __init__(self):
        t = threading.Thread(target=misc.command_watcher)
        t.daemon = True
        t.start()

        handlers = [
            (r'/rov/control', WebSocketHandler)
        ]

        tornado.web.Application.__init__(self, handlers)


if __name__ == "__main__":
    port = 5555
    print 'running websocket server...'
    app = App()
    server = tornado.httpserver.HTTPServer(app)
    server.listen(port)
    print 'server running on port %i...' % port
    tornado.ioloop.IOLoop.instance().start()
