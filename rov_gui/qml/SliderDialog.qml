import QtQuick 2.7
import QtQuick.Controls 1.4
import PopupMenuModel 0.1

Rectangle {
    id: sliderDialog

    property int type: PopupMenuModel.UNKNOWN
    property alias title: dialogTitle.text
    property alias sliderMaxValue: slider.maximumValue
    property alias sliderMinValue: slider.minimumValue
    property alias sliderDefaultValue: slider.value

    signal sliderValue(int value)
    signal cancel()

    color: "#80000000"
    border.width: 1
    border.color: "gray"
    radius: 10

    width: 0.25*parent.width
    height: 1.2*childrenRect.height

    Text {
        id: dialogTitle
        anchors.topMargin: appWin.buttonMargin
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        color: appWin.textColor
        font.pixelSize: appWin.textPixelSize
    }
    Slider {
        id: slider
        anchors.margins: 4*appWin.buttonMargin
        anchors.top: dialogTitle.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        stepSize: 1
        onValueChanged: {
            sliderValue.text = value
        }
    }
    Text {
        id: sliderValue
        anchors.top: slider.bottom
        anchors.horizontalCenter: slider.horizontalCenter
        color: appWin.textColor
        font.pixelSize: appWin.textPixelSize
    }

    CustomButtonStyle {
      id: btnStyle
    }
    Button {
        id: okButton
        anchors.top: sliderValue.bottom
        anchors.topMargin: appWin.buttonMargin
        anchors.right: sliderValue.horizontalCenter
        anchors.rightMargin: appWin.buttonMargin
        height: appWin.buttonHeight
        width: 0.4*parent.width
        style: btnStyle
        text: qsTr("OK")
        onClicked: {
            sliderDialog.sliderValue(slider.value)
            sliderDialog.visible = false
        }
    }
    Button {
        id: cancelButton
        anchors.top: okButton.top
        anchors.left: sliderValue.horizontalCenter
        anchors.leftMargin: appWin.buttonMargin
        height: okButton.height
        width: okButton.width
        style: btnStyle
        text: qsTr("Cancel")
        onClicked: {
            sliderDialog.visible = false
            sliderDialog.cancel()
        }
    }
}
