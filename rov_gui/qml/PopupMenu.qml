import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: template_frame

    property color popupColor: "#80000000"
    property color popupColorSelected: Qt.darker("gray")
    property int borderWidth: 1
    property int borderRadius: 10
    signal itemSelected(int index)

    width: 0.25*parent.width
    height: popupMenuModel.count*appWin.itemHeight

    Component {
        id: popupMenuDelegate
        Rectangle {
            id: item_frame
            x: template_frame.borderWidth
            y: template_frame.borderWidth
            width: popup_frame.width-2*template_frame.borderWidth
            height: appWin.itemHeight
            radius: template_frame.borderRadius
            color: template_frame.popupColor
            Text {
                id: item_text
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text: name
                color: appWin.textColor
                font.pixelSize: appWin.textPixelSize
            }
            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    template_frame.itemSelected(entryType)
                }
                onEntered: {
                    item_frame.color = template_frame.popupColorSelected
                }
                onExited: {
                    item_frame.color = template_frame.popupColor
                }
            }
        }
    }

    Rectangle {
        id: popup_frame
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: template_frame.height
        radius: template_frame.borderRadius
        border.width: template_frame.borderWidth
        border.color: "gray"
        color: template_frame.popupColor
        ListView {
            id: popup_list
            anchors.fill: parent
            model: popupMenuModel
            delegate: popupMenuDelegate
            clip: true
            boundsBehavior: Flickable.StopAtBounds
        }
    }
}
