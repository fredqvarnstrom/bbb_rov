import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Component {
  id: btnStyle
  ButtonStyle {
    background: Rectangle {
      radius: 3
      color: control.pressed?Qt.darker("gray"):"black"
    }
    label: Text {
      anchors.fill: parent
      horizontalAlignment: Text.AlignHCenter
      verticalAlignment: Text.AlignVCenter
      text: control.text
      color: appWin.textColor
      font.pixelSize: 0.3*control.height
    }
  }
}
