import QtQuick 2.7
import QtWebSockets 1.0
import QtGamepad 1.0
import QmlVlc 0.1
import PopupMenuModel 0.1

Item {
    id: cameraView

    function send(msg) {
        if (client.active) {
            client.sendTextMessage(msg)
            console.log('Sent: '+msg)
        } else {
            console.log('Cannot send: '+msg)
        }
    }

    Text {
        id: messageText

        function hide() {
            messageTextTimer.running = false
            visible = false
        }
        function show(msg) {
            messageTextTimer.running = true
            text = msg
        }

        z: 1
        anchors.centerIn: parent
        font.pixelSize: appWin.textPixelSize
        color: appWin.textColor
        Timer {
            id: messageTextTimer
            interval: 500
            repeat: true
            running: true
            onTriggered: {
                messageText.visible = !messageText.visible
            }
        }
    }
    Text {
        id: connectionStatusText

        function show(msg) {
            visible = true
            text = msg
        }

        z: 1
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        color: appWin.textColor
        font.pixelSize: 0.75*appWin.textPixelSize
    }

    VlcPlayer {
        id: vlcPlayer
        mrl: settings.videoUrl
        onStateChanged: {
            var msg = ""
            switch (state) {
            case VlcPlayer.NothingSpecial:
                msg = "Nothing special"
                break
            case VlcPlayer.Opening:
                messageText.show(qsTr("Loading ..."))
                msg = "Opening"
                break
            case VlcPlayer.Buffering:
                return //avoid too many logs
            case VlcPlayer.Playing:
                msg = "Playing"
                messageText.hide()
                if (!popupMenu.visible || !sliderDialog.visible) {
                    client.active = true
                }
                break
            case VlcPlayer.Paused:
                msg = "Paused"
                break
            case VlcPlayer.Stopped:
                msg = "Stopped"
                break
            case VlcPlayer.Ended:
                msg = "Ended"
                //restart playing
                vlcPlayer.play()
                break
            case VlcPlayer.Error:
                messageText.show(qsTr("libVLC: Error"))
                msg = "Error"
                break
            default:
                msg = "Unknown"
            }
            console.log("libVLC: "+msg)
        }
    }
    VlcVideoSurface {
        source: vlcPlayer;
        anchors.fill: parent
    }

    //web socket communication
    WebSocket {
        id: client
        url: settings.commandUrl
        onTextMessageReceived: {
            console.log("Received message: "+message)
        }
        onStatusChanged: {
            var msg = qsTr("Connection status: ")
            switch (status) {
            case WebSocket.Connecting:
                msg += qsTr("connecting")
                break
            case WebSocket.Open:
                msg += qsTr("open")
                break
            case WebSocket.Closing:
                msg += qsTr("closing")
                break
            case WebSocket.Closed:
                msg += qsTr("closed")
                break
            case WebSocket.Error:
                msg += qsTr("error")
                break
            default:
                msg = qsTr("unknown")
            }
            console.log(msg)
            connectionStatusText.show(msg)
        }
    }

    //gamepad manager
    Connections {
        target: GamepadManager
        onGamepadConnected: gamepad.deviceId = deviceId
    }

    Gamepad {
        id: gamepad

        function parseThumbStickAxis(axis) {
            return parseInt(127*(1+axis)/2.0, 10)
        }

        deviceId: GamepadManager.connectedGamepads.length > 0 ? GamepadManager.connectedGamepads[0] : -1
        onButtonXChanged: {
            cameraView.send(settings.commandButtonTemplate('X', value))
        }
        onButtonYChanged: {
            cameraView.send(settings.commandButtonTemplate('Y', value))
        }
        onButtonAChanged: {
            cameraView.send(settings.commandButtonTemplate('A', value))
        }
        onButtonBChanged: {
            cameraView.send(settings.commandButtonTemplate('B', value))
        }
        onButtonL2Changed: {
            //left trigger
            cameraView.send(settings.commandTriggerTemplate('Left',
                                                            parseInt(100*value, 10)))
        }
        onButtonR2Changed: {
            //right trigger
            cameraView.send(settings.commandTriggerTemplate('Right',
                                                            parseInt(100*value, 10)))
        }
        onButtonL3Changed: {
            //left stick
            cameraView.send(settings.commandThumbStickTemplate('Left',
                                                               parseThumbStickAxis(axisLeftX), parseThumbStickAxis(axisLeftY)))
        }
        onButtonR3Changed: {
            //right stick
            cameraView.send(settings.commandThumbStickTemplate('Right',
                                                               parseThumbStickAxis(axisRightX), parseThumbStickAxis(axisRightY)))
        }
        onButtonStartChanged: {
            console.log("Start buttton "+value)
            if (value) {
                popupMenu.visible = true
                client.active = false
            }
        }
    }
    PopupMenu {
        id: popupMenu
        anchors.centerIn: parent
        z: 1
        visible: false
        onItemSelected: {
            switch (index) {
            case PopupMenuModel.CONTROL_OPTIONS:
                popupMenuModel.toggleSubMenu()
                return //the popup menu stays visible
            case PopupMenuModel.LOG_OUT:
                 appWin.onLogout()
                 break
            case PopupMenuModel.EXIT:
                Qt.quit()
                break
            case PopupMenuModel.CANCEL:
                client.active = true
                break
            case PopupMenuModel.CONTROLLER_DEADBAND:
                cameraView.setupControllerDeadbandDialog()
                sliderDialog.visible = true
                popupMenuModel.toggleSubMenu()//hide submenu
                break
            case PopupMenuModel.CONTROL_RAMPING:
                cameraView.setupControlRampingDialog()
                sliderDialog.visible = true
                popupMenuModel.toggleSubMenu()//hide submenu
                break
            }
            popupMenu.visible = false
        }
    }

    function setupControlRampingDialog() {
        sliderDialog.type = PopupMenuModel.CONTROL_RAMPING
        sliderDialog.title = qsTr("Control Ramping")
        sliderDialog.sliderMinValue = 21
        sliderDialog.sliderMaxValue = 80
        sliderDialog.sliderDefaultValue = 60
    }
    function setupControllerDeadbandDialog() {
        sliderDialog.type = PopupMenuModel.CONTROLLER_DEADBAND
        sliderDialog.title = qsTr("Controller Deadband")
        sliderDialog.sliderMinValue = 1
        sliderDialog.sliderMaxValue = 20
        sliderDialog.sliderDefaultValue = 6
    }

    SliderDialog {
        id: sliderDialog
        anchors.centerIn: parent
        visible: false
        onSliderValue: {
            switch (type) {
            case PopupMenuModel.CONTROL_RAMPING:
                console.log("Control ramping value " + value)
                break
            case PopupMenuModel.CONTROLLER_DEADBAND:
                console.log("Controller deadband value " + value)
                break
            }
            client.active = true
        }
        onCancel: {
            client.active = true
        }
    }
}
