#ifndef LOGGER_H_
#define LOGGER_H_

#include <QDebug>
#include <QTextStream>
#include <QFile>
#include <QMutex>

class Logger : public QObject
{
    Q_OBJECT
public:
    /*! Get the only instance of the logger
     */
    static Logger& instance();
    /*! Get application folder
     */
    const QString& applicationFolder() const {
        return _appFolder;
    }
    /*! Get the current log file name. Used for testing purposes
     */
    const QString& logFileName() const {
        return _logFileName;
    }
    /*! Message handler. This is set in the Logger constructor as the message handler for all Qt log messages.
     * For testing purposes it is also a public method and can be set manually with:
     * qInstallMessageHandler(Logger::debugMessageHandler);
     */
    static void debugMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
    /*! Create folder inside the given path
     * \param path, path where the folder should be created
     * \param folderName, folder name
     */
    static void createFolder(const QString &path, const QString &folderName);
private slots:
    /*! Rename log file if one of the following conditions is true:
     * - the log file is not empty and the renaming has been forced (this might happen after midnight, when the renaming is automatically triggered)
     * - the log file is not empty and the last modification time is not today
     * When renaming the log file, the method makes sure that no existing log file is overriden by appending an integer number to the old log file name, i.e.
     * 'agent.log.yyyy-MM-dd.n'. This happens only if a file named 'agent.log.yyyy-MM-dd' already exists and should not happen usually.
     * This slot is public for testing purposes and should not be called directly.
     * \param force, if true the log file is renamed even if its modification date is today (in this case the date appended to the log file name is
     * decremented by one)
     */
    void renameLogFile(bool force = true);
private:
    Logger();
    Q_DISABLE_COPY(Logger);
    /*! Set a timer that triggers immediatelly after midnight in order to force the log file renaming.
     * The timer is setup only if the QApplication object is not NULL (which is the case in GUI-based applications).
     */
    void setupMidnightAlarm();
    static Logger _instance;
    const QString _appFolder;
    const QString _logFileName;
    QTextStream _textStream;
    QFile _logFile;
    QMutex _mutex;
};

#endif
