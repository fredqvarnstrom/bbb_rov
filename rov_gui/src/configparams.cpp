#include <QDebug>
#include "configparams.h"

ConfigParams::ConfigParams() : _configFile(":/config.xml")
{
    if (!_configFile.open(QIODevice::ReadOnly)) {
        qCritical() << "Cannot open configuration file";
    }
}

const QStringRef ConfigParams::nodeText(const char* name) const
{
    if (_configFile.isOpen()) {
        bool elementFound = false;
        _configFile.reset();
        _reader.setDevice(&_configFile);
        while (!_reader.atEnd()) {
            switch (_reader.readNext()) {
            case QXmlStreamReader::StartElement:
                if (0 == _reader.name().compare(name)) {
                    elementFound = true;
                }
                break;
            case QXmlStreamReader::Characters:
                if (elementFound && !_reader.isWhitespace()) {
                    return _reader.text();
                }
                break;
            default:
                ;
            }
        }
        //handle errors
        if (_reader.hasError()) {
            qCritical() << "Cannot read configuration file" << _reader.errorString();
        }
    }
    return QStringRef();
}

