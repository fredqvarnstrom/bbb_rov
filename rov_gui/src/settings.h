#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <QObject>
#include <QString>

class Settings : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString appName MEMBER _appName NOTIFY appNameChanged)
    Q_PROPERTY(QString appVersion MEMBER _appVersion NOTIFY appVersionChanged)
    Q_PROPERTY(QString commandUrl MEMBER _commandUrl NOTIFY commandUrlChanged)
    Q_PROPERTY(QString videoUrl MEMBER _videoUrl NOTIFY videoUrlChanged)
public:
    Settings();
    Q_INVOKABLE QString commandButtonTemplate(const QString &button, bool action);
    Q_INVOKABLE QString commandTriggerTemplate(const QString &trigger, int value);
    Q_INVOKABLE QString commandThumbStickTemplate(const QString &thumbStrick, int xValue, int yValue);
signals:
    void appNameChanged();
    void appVersionChanged();
    void commandUrlChanged();
    void videoUrlChanged();
private:
    QString _orgName;
    QString _appName;
    QString _appVersion;
    QString _commandUrl;
    QString _videoUrl;
    QString _commandButtonTemplate;
    QString _commandTriggerTemplate;
    QString _commandThumbStickTemplate;
};

#endif
