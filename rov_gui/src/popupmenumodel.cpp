#include <QKeySequence>
#include <QDebug>
#include "popupmenumodel.h"

PopupMenuModel::PopupMenuModel(QObject *parent) : QAbstractListModel(parent)
{
    Entry entry;
    entry.name = tr("Control Options");
    entry.type = CONTROL_OPTIONS;
    _entry.append(entry);
    entry.name = tr("Log Out");
    entry.type = LOG_OUT;
    _entry.append(entry);
    entry.name = tr("Exit");
    entry.type = EXIT;
    _entry.append(entry);
    entry.name = tr("Cancel");
    entry.type = CANCEL;
    _entry.append(entry);

    emit countChanged();
}

int PopupMenuModel::rowCount(const QModelIndex &/*parent*/) const
{
    return _entry.count();
}

QVariant PopupMenuModel::data(const QModelIndex &index, int role) const
{
    if ((index.row() < 0) || (index.row() > _entry.count())) {
        return QVariant();
    }
    const Entry &entry = _entry.at(index.row());
    QVariant out;
    switch (role) {
    case Name:
        out = entry.name;
        break;
    case EntryType:
        out = entry.type;
        break;
    default:
        ;
    }
    return out;
}

QHash<int,QByteArray> PopupMenuModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Name] = "name";
    roles[EntryType] = "entryType";
    return roles;
}

void PopupMenuModel::toggleSubMenu()
{
    static bool enable = true;
    emit layoutAboutToBeChanged();
    if (enable) {
        Entry entry;
        entry.name = "\t\t"+tr("Controller Deadband");
        entry.type = CONTROLLER_DEADBAND;
        _entry.insert(1, entry);
        entry.name = "\t\t"+tr("Control Ramping");
        entry.type = CONTROL_RAMPING;
        _entry.insert(2, entry);
    } else {
        for (int i = 0; i < _entry.size(); ++i) {
            switch(_entry.at(i).type) {
            case CONTROLLER_DEADBAND:
            case CONTROL_RAMPING:
                _entry.removeAt(i);
                i = 0;//restart search
            default:
                ;
            }
        }
    }
    enable = !enable;
    emit layoutChanged();
    emit countChanged();
}
