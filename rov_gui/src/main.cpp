#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "settings.h"
#include "QmlVlc.h"
#include "QmlVlcConfig.h"
#include "popupmenumodel.h"

int main(int argc, char *argv[])
{
    RegisterQmlVlc();
    QmlVlcConfig& config = QmlVlcConfig::instance();
    config.enableAdjustFilter( true );
    config.enableMarqueeFilter( true );
    config.enableLogoFilter( true );
    config.enableDebug( true );

    //expose EntryTypes enum
    qmlRegisterType<PopupMenuModel>("PopupMenuModel", 0, 1, "PopupMenuModel");

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    //set properties
    QQmlContext *context = engine.rootContext();//registered properties are available to all components
    if (NULL != context) {
        Settings *settings = new Settings();
        context->setContextProperty("settings", settings);
        PopupMenuModel *popupMenuModel = new PopupMenuModel();
        context->setContextProperty("popupMenuModel", popupMenuModel);
    }

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    app.setQuitOnLastWindowClosed(true);
    return app.exec();
}
