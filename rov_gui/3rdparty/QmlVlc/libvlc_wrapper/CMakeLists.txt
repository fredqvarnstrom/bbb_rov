file( GLOB SOURCE_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
    [^.]*.cpp
    [^.]*.h
    )

add_library( libvlc_wrapper STATIC ${SOURCE_FILES} )
set_target_properties( libvlc_wrapper PROPERTIES POSITION_INDEPENDENT_CODE ON )
target_include_directories( libvlc_wrapper PUBLIC ${LIBVLC_INCLUDE_DIR} )
target_link_libraries( libvlc_wrapper ${LIBVLC_LIBRARY} )
